package com.time.table;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import static android.widget.Toast.LENGTH_LONG;

public class register extends AppCompatActivity {

    EditText name=findViewById(R.id.name);
    EditText email=findViewById(R.id.email);
    EditText password=findViewById(R.id.password);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Button submit=findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url="http://192.168.43.209/timetable/register.php";
                StringRequest sr=new StringRequest(1, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(register.this,response,Toast.LENGTH_SHORT).show();
                                /*clear form after response*/


                                startActivity(new Intent(register.this,MainActivity.class));
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> map =new HashMap<>();
                        map.put("namekey", name.getText().toString());
                        map.put("emailkey", email.getText().toString());
                        map.put("passwordkey", password.getText().toString());
                        return map;
                    }
                };
                RequestQueue rq= Volley.newRequestQueue(register.this);
                rq.add(sr);
            }
        });
    }
}
